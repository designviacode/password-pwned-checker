<?php
  /**
  * Simple method to use the API from https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/
  * Written by Jim Westergren and released to public domain
  * @return int count
  */
  function checkpwnedPasswords(string $password) : int {
    $sha1 = strtoupper(sha1($password));
    $data = file_get_contents('https://api.pwnedpasswords.com/range/'.substr($sha1, 0, 5));
    if (strpos($data, substr($sha1, 5))) {
      $data = explode(substr($sha1, 5).':', $data);
      $count = (int) $data[1];
    }
    return $count ?? 0;
  }

  isset($_GET["password"]) && $result = checkpwnedPasswords($_GET["password"]);
?>

<html>
<head>
  <title>Is your Password pwned! | passwordpwned.com</title>
  <style>
    *:before, *:after {
      box-sizing: border-box;
    }
    html, body {
      font-family: Arial;
      margin: 0;
      padding: 0;
    }
    input, button {
      border: 1px solid #ddd;
      border-radius: 2.5px;
      font-family: Arial, sans-serif;
      letter-spacing: 1px;
      margin: 0 2.5px 2.5px;
      outline: 0;
      padding: 10px;
      transition: 0.2s ease-in;
    }
    input:focus, button:focus {
      border-color: #aaa;
    }
    input {
      min-width: 200px;
      font-size: 1.2rem;
    }
    button {
      background-color: white;
      color: #444;
      cursor: pointer;
      font-size: 1.2rem;
      min-width: 100px;
    }
    ::placeholder {
      color: #bbb;
      font-family: Arial;
      font-weight: 300;
      font-size: 16px;
    }
    ::selection {
      background-color: springgreen;
      color: white;
    }
    h1 {
      font-size: 24px;
      font-weight: 500;
      margin: 0;
      margin-bottom: 7.5px;
    }
    h1 > span {
      font-weight: 700;
    }

    .app-container {
      display: flex;
      flex: 1 1 auto;
      align-items: flex-start;
      justify-content: center;
      min-height: 100vh;
      min-width: 100vw;
      animation: documentOnLoad 1.5s ease;
      animation-iteration-count: 1;
      animation-fill-mode: forwards;
    }
    .hero {
      padding: 2.5px;
      padding-top: 120px;
    }
    .hero--title {
      padding: 10px;
    }
    .hero--title h1 {
      font-size: 2em;
    }
    .hero--title h1:first-child {
      font-size: 2.5em;
    }
    .hero--title h1:nth-child(2) {
      font-size: 1.8em;
    }

    .hero--content {
      padding: 10px;
    }
    .form--password-pwned {
      display: flex;
      flex-direction: row;
      padding: 10px;
    }
    .form-input--password {
      /* min-width: 320px; */
    }
    .form-action--password:hover, .form-action--password:focus {
      opacity: 0.5;
    }



    @keyframes documentOnLoad {
      0% {
        opacity: 0;
        transform: translateY(50px);
      }
      50% {
        transform: translateY(0);
      }
      90% {
        opacity: 1;
      }
    }
  </style>
</head>
<body>
  <div class="app-container">
    <div class="hero">
      <div class="hero--title">
        <h1>Welcome!</h1>
        <h1>Let's find out if your password has been <span>pwned</span>!</h1>
      </div>
      <div class="hero--form">
        <form action="index.php" method="get" class="form--password-pwned">
          <input type="text" placeholder="Enter any password.." name="password" class="form-input--password" spellcheck="false">
          <button type="submit" class="form-action--password">Check</button>
        </form>
      </div>

      <?php if (isset($_GET["password"])): ?>
        <div class='hero--content'>
          <h1>Password Value: <b><?php echo isset($_GET['password']); ?></b></h1>
          <h1>Password pwned count: <b><?php echo isset($result); ?></b></h1>
        </div>
      <?php endif; ?>
    </div>
  </div>
</body>
</html>